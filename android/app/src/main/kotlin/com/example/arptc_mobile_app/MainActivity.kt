package com.example.aduitor_mobile

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.telephony.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity : FlutterActivity() {

    private val networkChannel = "mobile.arptc.dev/network"

    @RequiresApi(Build.VERSION_CODES.P)
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, networkChannel).setMethodCallHandler { call, result ->
            if (call.method == "getNetworkInfo") {
                result.success(getNetworkQualityInfo())
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun getNetworkQualityInfo(): List<Map<String, String?>> {

        val telephonyManager: TelephonyManager = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return listOf(emptyMap())
        }
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.

                /*val dataNetworkType: String = when (telephonyManager.dataNetworkType) {
            TelephonyManager.NETWORK_TYPE_IDEN -> "2G"
            TelephonyManager.NETWORK_TYPE_HSPAP -> "3G"
            TelephonyManager.NETWORK_TYPE_LTE -> "4G"
            else -> "OTHER"*/
        //}

        val map : MutableList<Map<String, String?>> = telephonyManager.allCellInfo.map { cellInfo ->
            val cellInfoLte = cellInfo as CellInfoLte
            mapOf(
                    "alphaShort" to cellInfoLte.cellIdentity.operatorAlphaShort.toString(),
                    "mnc" to cellInfoLte.cellIdentity.mncString,
                    "mcc" to cellInfoLte.cellIdentity.mccString,
                    "signalStrength" to cellInfoLte.cellSignalStrength.dbm.toString()
            )
        }.toMutableList()

       // map.add(mapOf("dataNetworkType" to dataNetworkType))

        return map
    }
}
