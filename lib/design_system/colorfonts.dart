import 'package:flutter/material.dart';

class ThemeConstants {
  ThemeConstants._();

//Couleur New Orleans
  static final newOrleansDefault = 0xFFF2D19F;
  static const MaterialColor newOrleans = const MaterialColor(
    0xFFF2D19F,
    const <int, Color>{
      50: const Color(0xFFFBF2E9),
      100: const Color(0xFFFBF0E1),
      200: const Color(0xFFF9EBD6),
      300: const Color(0xFFF8E5C9),
      400: const Color(0xFFF5DCB7),
      500: const Color(0xFFF2D19F),
      600: const Color(0xFFDEB577),
      700: const Color(0xFFCD9D54),
      800: const Color(0xFFB8873E),
      900: const Color(0xFF97723A),
    },
  );

// Couleur Goblin
  static final goblinDefault = 0xFF3E8859;
  static const MaterialColor goblin = const MaterialColor(
    0xFF3E8859,
    const <int, Color>{
      50: const Color(0xFFCCE8D6),
      100: const Color(0xFFBBE0C9),
      200: const Color(0xFFA5D6B7),
      300: const Color(0xFF86C89E),
      400: const Color(0xFF5EB67E),
      500: const Color(0xFF3E8859),
      600: const Color(0xFF3B6D4D),
      700: const Color(0xFF345B42),
      800: const Color(0xFF2E4B39),
      900: const Color(0xFF283F31),
    },
  );

// Couleur Amazon
  static final amazonDefault = 0xFF317046;
  static const MaterialColor amazon = const MaterialColor(
    0xFF317046,
    const <int, Color>{
      50: const Color(0xFFC5E6D0),
      100: const Color(0xFFB2DDC1),
      200: const Color(0xFF99D2AC),
      300: const Color(0xFF77C390),
      400: const Color(0xFF4CAD6C),
      500: const Color(0xFF317046),
      600: const Color(0xFF2F5A3D),
      700: const Color(0xFF2A4B35),
      800: const Color(0xFF253E2D),
      900: const Color(0xFF203427),
    },
  );

// Couleur Celtic
  static final celticDefault = 0xFF173D24;
  static const MaterialColor celtic = const MaterialColor(
    0xFF173D24,
    const <int, Color>{
      50: const Color(0xFFB6E3C6),
      100: const Color(0xFF9DDAB2),
      200: const Color(0xFF7DCE99),
      300: const Color(0xFF51BD77),
      400: const Color(0xFF348A52),
      500: const Color(0xFF173D24),
      600: const Color(0xFF173120),
      700: const Color(0xFF15281B),
      800: const Color(0xFF122118),
      900: const Color(0xFF101C14),
    },
  );

// Couleur Grey
  static final greyDefault = 0xFF9E9E9E;
  static const MaterialColor grey = const MaterialColor(
    0xFF9E9E9E,
    const <int, Color>{
      50: const Color(0xFFF9F9F9),
      100: const Color(0xFFF3F3F3),
      200: const Color(0xFFEEEEEE),
      300: const Color(0xFFDFDFDF),
      400: const Color(0xFFBBBBBB),
      500: const Color(0xFF9E9E9E),
      600: const Color(0xFF747474),
      700: const Color(0xFF606060),
      800: const Color(0xFF424242),
      900: const Color(0xFF212121),
    },
  );

  static final double bodyRegularFontSize = 14.0;
  static final double header6FontSize = 16.0;
  static final double header5FontSize = 20.0;
  static final double header4FontSize = 25.0;
  static final double header3FontSize = 31.25;
  static final double header2FontSize = 39.06;
  static final double header1FontSize = 48.83;
  static final double bouttonTextSize = 16;


  static final String gilroy = 'Gilroy';
  static final String poppins = 'Poppins';


  static final TextStyle skipText = TextStyle(
                        color: Color.fromRGBO(66, 66, 66, 1), fontFamily: gilroy, fontSize: 16,letterSpacing: 0, fontWeight: FontWeight.normal,height: 1);
  static final TextStyle bouttonTextVert = TextStyle(
      color: Color.fromRGBO(248, 248, 248, 1), fontSize: bouttonTextSize, fontFamily: gilroy, letterSpacing: 0, fontWeight: FontWeight.normal);
  static final TextStyle bouttonTextOrange = TextStyle(
      color: ThemeConstants.newOrleans[800], fontSize: bouttonTextSize, fontFamily: gilroy, letterSpacing: 0, fontWeight: FontWeight.normal);
  static final TextStyle headline1 = TextStyle(
      color: Colors.black, fontSize: header1FontSize, fontFamily: gilroy);
  static final TextStyle headline2 = TextStyle(
      color: Colors.black, fontSize: header2FontSize, fontFamily: gilroy);
  static final TextStyle headline3 = TextStyle(
      color: Colors.black, fontSize: header3FontSize, fontFamily: gilroy);
  static final TextStyle headline4 = TextStyle(
      color: Colors.black, fontSize: header4FontSize, fontFamily: gilroy, fontWeight: FontWeight.bold);
  static final TextStyle headline5 = TextStyle(
      color: Colors.black, fontSize: header5FontSize, fontFamily: gilroy);
  static final TextStyle headline6 = TextStyle(
      color: Colors.black, fontSize: header6FontSize, fontFamily: gilroy);
  static final TextStyle bodyRegularStyleWhite = TextStyle(
      color: Colors.white, fontSize: bodyRegularFontSize, fontFamily: poppins, fontWeight: FontWeight.w400);
  static final TextStyle bodyRegularStyle = TextStyle(
      color: Colors.grey, fontSize: bodyRegularFontSize, fontFamily: poppins, fontWeight: FontWeight.w400);
// opté pour la couleur noir par défaut.

}
