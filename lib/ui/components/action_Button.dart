import 'package:flutter/material.dart';

class ActionBoutton extends StatelessWidget {
  final Color couleurBoutton;
  final TextStyle styleText;
  final String text;

  const ActionBoutton(
      {required this.couleurBoutton,
      required this.text,
      required this.styleText});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: couleurBoutton,
      ),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            text,
            textAlign: TextAlign.left,
            style: styleText,
          ),
        ],
      ),
    );
  }
}
