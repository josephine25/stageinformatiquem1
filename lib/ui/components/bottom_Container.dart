import 'package:flutter/material.dart';

// classe pour l'indicateur 3 points des silides.
class BottomContainer extends StatelessWidget {
  final double hauteur;
  final Widget child;
  

  const BottomContainer(
      {required this.hauteur,
      required this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: hauteur,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50), topRight: Radius.circular(50))),
      padding: EdgeInsets.symmetric(horizontal: 35, vertical: 10),
      child: child
      
    );
  }
}
