import 'package:flutter/material.dart';
import 'package:aduitor_mobile/design_system/colorfonts.dart';

// classe pour l'indicateur 3 points des slides.
class Indicator extends StatelessWidget {
  final bool activ;

  const Indicator({required this.activ});
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 3.5),
      height: 7.0,
      width: activ ? 7.5 : 7.0,
      decoration: BoxDecoration(
        color: activ ? ThemeConstants.goblin : Color(0xFFCCE8D6),
        borderRadius: new BorderRadius.all(Radius.elliptical(100, 100)),
      ),
    );
  }
}
