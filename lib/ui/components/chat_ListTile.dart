//prend les params suivants en utilisant les ListTitle:
//Name (title) , date +  notif (trailing), last message+ acknowlegment(pour le subtitle), photo (leading)
import 'package:aduitor_mobile/design_system/colorfonts.dart';
import 'package:flutter/material.dart';

// classe pour l'indicateur 3 points des silides.
class ChatListTile extends StatelessWidget {
  final String  nom;
  final String  prenom;
  final String  derniermes;
  final String  heure;
  final int nonlu;
  final AssetImage photoprofil;

  
  const ChatListTile(
      {required this.heure,
      required this.nonlu, required this.nom, required this.prenom, required this.derniermes,required this.photoprofil  });
  @override
  Widget build(BuildContext context) {
    return Container(
    child: ListTile(
      trailing: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(heure, style:ThemeConstants.bodyRegularStyle,),//restyle
            nonlu == 0
            ?Text("")
            :Container(
              
              width: 30.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: ThemeConstants.goblin
              ),
              child: 
              Center(child: Text(nonlu.toString(),style:ThemeConstants.bodyRegularStyleWhite,)))
          ],
        )),
      leading: CircleAvatar(
        backgroundImage: photoprofil,
      ),
      title: Text(nom + prenom, style: ThemeConstants.bodyRegularStyle,),//restyle
      subtitle: Text(derniermes,style:ThemeConstants.bodyRegularStyle, ),//restyle
    ),
  );
  }
}
