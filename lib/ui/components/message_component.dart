import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  final MessageType type;
  final String text;

  const Message({required this.type, required this.text});

  @override
  Widget build(BuildContext context) {
    var baseColor = computeBaseColor(type);
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.warning_amber_rounded,
                  color: baseColor.shade900,
                )),
            Expanded(
              flex: 6,
              child: Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  height: 1.5,
                  color: baseColor.shade900,
                ),
              ),
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        color: baseColor.shade100,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}

MaterialColor computeBaseColor(MessageType messageType) {
  switch (messageType) {
    case MessageType.INFO:
      return Colors.blue;
    case MessageType.WARNING:
      return Colors.yellow;
    case MessageType.ERROR:
      return Colors.red;
    default:
      return Colors.grey;
  }
}

enum MessageType { ERROR, WARNING, INFO }
