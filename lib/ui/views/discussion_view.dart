import 'package:aduitor_mobile/design_system/colorfonts.dart';
import 'package:aduitor_mobile/ui/components/chat_ListTile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class Discussion extends StatefulWidget {
  const Discussion({Key? key}) : super(key: key);

  @override
  _DiscussionState createState() => _DiscussionState();
}

int ind = 0;
//retableau image
//AssetImage('assets/images/Useravatar ($ind).png'),
var nom = [
  "Nguyen",
  "Fox",
  "Bell",
  "Warren",
  "Edwards",
  "Coopers",
  "Jones",
  "Flores",
  "Russel"
];





class _DiscussionState extends State<Discussion> {
  @override
  Widget build(BuildContext context) {
    //condition deuxieme page
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(children: <Widget>[
        SizedBox(height: 10.0),
        Container(
            height: 50.0,
            decoration: BoxDecoration(
                color: ThemeConstants.grey[100],
                borderRadius: BorderRadius.all(Radius.circular(10))),
            margin: EdgeInsets.all(14.0),
            child: TextField(
              style: ThemeConstants.bodyRegularStyle,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  enabledBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: "Rechercher dans la discussion",
                  icon: Icon(FeatherIcons.search,
                      color: ThemeConstants.grey[400])),
            )),
        Container(
          height: 540.0,
          child: ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: 9,
            itemBuilder: (BuildContext context, int index) {
              var nonlu = [ 0,0,2,0,0,0,0,2,0,];
              var heure = ["19:24","8:45","13:24","14:45","17:00","16:45","8:12","15:24","17:12"];
              var derniermes = [
  "Vous avez envoyé 35.000",
  "Sollicitudin quis eu sed volutpat eu ablor...",
  "Sollicitudin quis eu sed volutpat eu ablor...",
  "Vous reçu envoyé 18.000",
  "Sollicitudin quis eu sed volutpat eu itemos...",
  "Sollicitudin quis eu sed volutpat eu itemos...",
  "At platea euismod ultrices quam",
  "Sollicitudin quis eu sed volutpat eu ablor...",
  "Vous reçu envoyé 18.000"
];
var prenom = [
  "Savannah",
  "Robert",
  "Jérôme",
  "Yvonne",
  "Ralph" ,
  "Jane",
  "Samira",
  "Albert",
  "Dianne"
];

var nom = [
  "Nguyen",
  "Fox",
  "Bell",
  "Warren",
  "Edwards",
  "Coopers",
  "Jones",
  "Flores",
  "Russel"
];

              ChatListTile chatListTile = new ChatListTile(
                  heure: heure[index],
                  nonlu: nonlu[index],
                  nom: nom[index],
                  prenom: prenom[index],
                  derniermes: derniermes[index],
                  photoprofil: AssetImage(
                      'assets/images/user-avatar.png'));
              return chatListTile;
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ),
        )
      ]),
    );
  }
}
