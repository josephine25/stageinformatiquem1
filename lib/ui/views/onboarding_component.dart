import 'package:aduitor_mobile/design_system/colorfonts.dart';
import 'package:aduitor_mobile/flow/onboarding/ui/views/welcome_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget onBoarding(Text titreprincipal, Text description, Text buttonText,
    BoxDecoration boxDecoration, AssetImage lienImage, BuildContext context,
     Color col1, Color col2, Color col3) {
  return Scaffold(
    body: Container(
        decoration: BoxDecoration(
          color: ThemeConstants.newOrleans[50],
        ),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              skipButton(),
              welcomeImage(lienImage),
              Stack(
                children: [
                  Container(
                    height: 305,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50))),
                    padding: EdgeInsets.symmetric(horizontal: 35, vertical: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          height: 3,
                        ),
                        marqueur(col1, col2, col3),
                        SizedBox(
                          height: 3,
                        ),
                        Center(
                          child: titreprincipal,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        description,
                        SizedBox(
                          height: 20,
                        ),
                        startButton(
                            buttonText, boxDecoration, context, lienImage),
                      ],
                    ),
                  ),
                ],
              ),
            ])),
  );
}

GestureDetector startButton(Text buttonText, BoxDecoration boxDecoration,
    BuildContext context, AssetImage lienImage) {
  return GestureDetector(
    onTap: () {
      if (lienImage == AssetImage('assets/images/manage_money.png')) {
        Navigator.push(
            context, CupertinoPageRoute(builder: (context) => WelcomeScreen()));
      } else if (lienImage == AssetImage('assets/images/plan_transferts.png')) {
        Navigator.push(
            context, CupertinoPageRoute(builder: (context) => WelcomeScreen()));
      } else if (lienImage ==
          AssetImage('assets/images/contribute_money.png')) {
        Navigator.push(
            context, CupertinoPageRoute(builder: (context) => WelcomeScreen()));
      } else {
        Navigator.push(
            context, CupertinoPageRoute(builder: (context) => WelcomeScreen()));
      }
    },
    child: Container(
      decoration: boxDecoration,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[buttonText],
      ),
    ),
  );
}

Container welcomeImage(AssetImage lienImage) {
  return Container(
    width: 345,
    height: 345,
    child: Image(image: lienImage),
  );
}
Container marqueur(Color col1, Color col2, Color col3){
 
  // visibilité ou non des elipses
  return Container(
    height: 9.0,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            height: 7,
            width: 7,
            decoration: new BoxDecoration(
              color: col1,
              border: Border.all(color: col1, width: 0.0),
              borderRadius: new BorderRadius.all(Radius.elliptical(100, 100)),
            ),
           
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            height: 7,
            width: 7,
            decoration: new BoxDecoration(
              color: col2,
              border: Border.all(color: col2, width: 0.0),
              borderRadius: new BorderRadius.all(Radius.elliptical(100, 100)),
            ),
           
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            height: 7,
            width: 7,
            //margin: EdgeInsets.only(top: 40, left: 40, right: 40),
            decoration: new BoxDecoration(
              color: col3,
              border: Border.all(color: col3, width: 0.0),
              borderRadius: new BorderRadius.all(Radius.elliptical(100, 100)),
            ),
           
          ),
      ],
    ),
  );
}
Container skipButton() {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
    child: Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Text(
          'Passer',
          textAlign: TextAlign.left,
          style: TextStyle(
              color: Color.fromRGBO(66, 66, 66, 1),
              fontFamily: 'Gilroy',
              fontSize: 16,
              letterSpacing: 0,
              fontWeight: FontWeight.normal,
              height: 1),
        ),
      ],
    ),
  );
}
/*
Map  map0 = {nom: "Nguyen", prenom:"Savannah", derniermes:"Vous avez envoyé 35.000", heure:"19:24"};
Map  map1 = {nom: "Fox", prenom:"Robert", derniermes:"Sollicitudin quis eu sed volutpat eu ablor...", heure:"8:45"};
Map  map2 = {nom: "Bell", prenom:"Jérôme", derniermes:"Sollicitudin quis eu sed volutpat eu ablor...", heure:"13:24"};
Map  map3 = {nom: "Warren", prenom:"Yvonne", derniermes:"Sollicitudin quis eu sed volutpat eu itemos...", heure:"14:45"};
Map  map4 = {nom: "Edwards", prenom:"Ralph", derniermes:"Sollicitudin quis eu sed volutpat eu itemos...", heure:"17:00",};
Map  map5 = {nom: "Coopers", prenom:"Jane", derniermes:"At platea euismod ultrices quam", heure:"16:45"};
Map  map6 = {nom: "Jones", prenom:"Samira", derniermes:"Sollicitudin quis eu sed volutpat eu ablor...", heure:"8:12"};
Map  map7 = {nom: "Flores", prenom:"Albert", derniermes:"Sollicitudin quis eu sed volutpat eu itemos...", heure:"15:24"};
Map  map8 = {nom: "Russel", prenom:"Dianne", derniermes:"Vous reçu envoyé 18.000", heure:"17:12"};
var maps= [map0,map1,map2,map3,map4,map5,map6,map7,map8];
 */