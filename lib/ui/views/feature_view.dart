import 'package:flutter/material.dart';

class FeatureView extends StatelessWidget {
  final String title;
  final Widget? content;

  FeatureView(this.title, {this.content});

  @override
  Widget build(BuildContext context) {
    return content ?? _buildDefaultContent();
  }

  static Column _buildDefaultContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image(
          image: AssetImage('assets/images/under-construction.png'),
        ),
        Text(
          "Page en cours de construction",
          style: TextStyle(fontFamily: 'comic sans'),
        )
      ],
    );
  }
}
