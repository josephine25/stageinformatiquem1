
/*import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';
import 'package:flutter/widgets.dart';
import 'package:aduitor_mobile/bloc/network_quality/bloc.dart';
import 'package:aduitor_mobile/bloc/network_quality/events.dart';
import 'package:flutter_reachability/flutter_reachability.dart';
import 'package:permission_handler/permission_handler.dart';


const String TAG = "BackGround_Work";

class TestStagiaire extends StatefulWidget {
  const TestStagiaire({ Key? key }) : super(key: key);

  @override
  _TestStagiaireState createState() => _TestStagiaireState();
}


class _TestStagiaireState extends State<TestStagiaire> {
  String _networkStatus = 'Unknown';
  late StreamSubscription<NetworkStatus> subscription;
  int _counterValue = 0;
  
    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listenNetworkStatus();
    Workmanager().initialize( // le remettre au niveau du main des fichier.dart
      callbackDispatcher,
      isInDebugMode: false 
      );
    //tourne periodiquement
    Workmanager().registerPeriodicTask(
      TAG,
      "simplePeriodicTask",
      initialDelay: Duration(minutes: 15),
    );
    loalData();
  }
   _listenNetworkStatus()async {
    if(Platform.isAndroid) {
      await Permission.phone.request();
    }
    subscription = FlutterReachbility().onNetworkStateChanged.listen((event) {

      setState(() {
        _networkStatus = "${event}";
      });
    });
  }

  _currentNetworkStatus() async {
    if(Platform.isAndroid) {
      await Permission.phone.request();
    }
    NetworkStatus status = await FlutterReachbility().currentNetworkStatus();
    switch(status) {
      case NetworkStatus.unreachable:
      //unreachable
      case NetworkStatus.wifi:
      //wifi
      case NetworkStatus.mobile2G:
      //2g
      case NetworkStatus.moblie3G:
      //3g
      case NetworkStatus.moblie4G:
      //4g
      case NetworkStatus.moblie5G:
      //5h
      case NetworkStatus.otherMoblie:
      //other
    }
    setState(() {
      _networkStatus = status.toString() ;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    subscription.cancel();
  }

  void loalData() async {
    _counterValue =
        await BackGroundWork.instance._getBackGroundCounterValue();
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
     final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 35),
          ElevatedButton(
            style: style,
            onPressed: () {
            //loalData();
            },
            child: Text('Counter Value: ${_counterValue}'),
          ),
        Text('Running on: $_networkStatus\n'),
        ],
        
      ),
    );
  }
}


//Appelle les tâches à exécuter ne bacground
void callbackDispatcher() {

  Workmanager().executeTask((task, inputData) async {
    
    print("********************************[Workmanager]Je tourne***************************");
    final NetworkQualityBloc networkQualityBloc = new NetworkQualityBloc();
    BackGroundWork.instance._network(networkQualityBloc);
    int value =  await BackGroundWork.instance._getBackGroundCounterValue();
    //Test du compteur gardé en background pour une vue en interface
    BackGroundWork.instance._loadCounterValue(value + 2);
    print("********************************[Workmanager] Valeur du compteur : {$value}");
    return Future.value(true);
  });
}
//classe pour le travail arriere plan
class BackGroundWork {
  BackGroundWork._privateConstructor();

  static final BackGroundWork _instance =
  BackGroundWork._privateConstructor();

  static BackGroundWork get instance => _instance;
  //Envoie de l'evenement
  _network(NetworkQualityBloc networkQualityBloc){
           networkQualityBloc.add(NetworkQualityEvent.SCHEDULER_TRIGGERED);
           print("********************************[Workmanager]Je tourne apres aussi Scheduler***************************");
           print(networkQualityBloc);
  }
  //Gestion du coumpteur
  _loadCounterValue(int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('BackGroundCounterValue', value);
  }

  Future<int> _getBackGroundCounterValue() async {
    //Stock les informations dans le shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return bool
    int counterValue = prefs.getInt('BackGroundCounterValue') ?? 0;
    return counterValue;
  }
}*/