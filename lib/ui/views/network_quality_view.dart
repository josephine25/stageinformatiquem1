import 'package:aduitor_mobile/bloc/network_quality/bloc.dart';
import 'package:aduitor_mobile/bloc/network_quality/states.dart';
import 'package:aduitor_mobile/model/network_quality/network_quality.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:aduitor_mobile/design_system/colorfonts.dart';

class NetworkQualityView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NetworkQualityBloc, NetworkQualityState>(
        builder: (context, state) {
          print("State: " + state.toString());
      if (state is NetworkQualityLoaded) {
        GlobalNetworkInfo networkQuality = state.networkQuality;
        return Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: networkQualityElements(networkQuality),
          ),
        );
      } else if (state is NetworkQualityLoading) {
        return Padding(
          padding: const EdgeInsets.all(20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
             // crossAxisAlignment: CrossAxisAlignment.baseline,
              children: [

                const CircularProgressIndicator(),
              ],
            ),
          ),
        );
      }
      throw Exception("Such state is not available for this widget");
    });
  }

  List<Widget> networkQualityElements(GlobalNetworkInfo networkQuality) {
    List<Widget> list = [];
    /*if (networkQuality.local) {
      list.add(Message(
          type: MessageType.WARNING,
          text:
              "Les informations affichées sont locales et donc peu fiables. Veuillez activer internet pour avoir des informations plus fiables"));
    }*/
    list.add(SingleTopicQualityComponent(
        title: "Voix et SMS",
        quality: Map.fromIterable(networkQuality.voiceAndSms,
            key: (e) => e.operatorName, value: (e) => e.signalQuality),
        icon: Icon(Icons.phone)));
    /*list.add(SingleTopicQualityComponent(
        title: "Internet 3G, 4G, 5G",
        quality: networkQuality.internet,
        icon: Icon(FontAwesome5.globe_americas)));*/
    return list;
  }
}

class SingleTopicQualityComponent extends StatelessWidget {
  final Map<String, double> quality;
  final String title;
  final Icon icon;

  SingleTopicQualityComponent(
      {required this.title, required this.quality, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25, top: 25),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _widgetChildrens(quality)),
    );
  }

  List<Widget> _widgetChildrens(Map<String, double> quality) {
    List<Widget> list = [
      Row(children: [
        icon,
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              title,
              style: ThemeConstants.headline1,
            ),
          ),
        )
      ])
    ];
    list.addAll(
      quality.entries.map((entry) {
        return Padding(
          padding: const EdgeInsets.only(top: 15.0),
          child: Row(children: [
            SizedBox(width: 10),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      LinearPercentIndicator(
                          percent: entry.value,
                          lineHeight: 10.0,
                          progressColor: computeColor(entry.value),
                          animation: true),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0, top: 4),
                        child: Text(entry.key, style: ThemeConstants.headline2),
                      )
                    ]),
              ),
            )
          ]),
        );
      }),
    );
    return list;
  }

  MaterialColor computeColor(double percentage) {
    if (percentage >= 0 && percentage < 0.33) {
      return ThemeConstants.amazon;
    } else if (percentage >= 0.33 && percentage < 0.66) {
      return ThemeConstants.newOrleans;
    } else if (percentage >= 0.66 && percentage <= 1) {
      return ThemeConstants.grey;
    } else {
      return Colors.grey;
    }
  }
}
