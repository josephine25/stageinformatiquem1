
import 'package:aduitor_mobile/bloc/network_quality/bloc.dart';
import 'package:aduitor_mobile/flow/onboarding/ui/views/welcome_screen.dart';
import 'package:aduitor_mobile/design_system/colorfonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workmanager/workmanager.dart';

import 'scheduler.dart';
const String TAG = "BackGround_Work";

void main() {
  // TODO: implement initState
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager().initialize(
      callbackDispatcher, // The top level function, aka callbackDispatcher
      isInDebugMode: false // This should be false
      );
  runApp(MyApp());

}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => NetworkQualityBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Application Mobile ARPTC',
        theme: ThemeData(
          primarySwatch: ThemeConstants.newOrleans,
        ),
        home: WelcomeScreen(),
      ),
    );
  }
}
callbackDispatcher(){
  Scheduler.callbackDispatcher();
}