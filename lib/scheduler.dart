import 'package:workmanager/workmanager.dart';
import 'bloc/network_quality/bloc.dart';
import 'bloc/network_quality/events.dart';

class Scheduler {
  static void callbackDispatcher() {
    Workmanager().executeTask((task, inputData) async {
       print("********************************[Workmanager]Je tourne***************************");
       final NetworkQualityBloc networkQualityBloc = new NetworkQualityBloc();
       networkQualityBloc.add(NetworkQualityEvent.SCHEDULER_TRIGGERED);
       print("********************************[Workmanager]Je tourne apres aussi Scheduler***************************");
       print(networkQualityBloc);
       return Future.value(true);
    });
  }
}

