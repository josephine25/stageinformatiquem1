// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_quality.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GlobalNetworkInfo _$GlobalNetworkInfoFromJson(Map<String, dynamic> json) {
  return GlobalNetworkInfo(
    voiceAndSms: (json['voiceAndSms'] as List<dynamic>)
        .map((e) => OperatorAvgQuality.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GlobalNetworkInfoToJson(GlobalNetworkInfo instance) =>
    <String, dynamic>{
      'voiceAndSms': instance.voiceAndSms.map((e) => e.toJson()).toList(),
    };

OperatorAvgQuality _$OperatorAvgQualityFromJson(Map<String, dynamic> json) {
  return OperatorAvgQuality(
    json['operatorName'] as String,
    (json['signalQuality'] as num).toDouble(),
  );
}

Map<String, dynamic> _$OperatorAvgQualityToJson(OperatorAvgQuality instance) =>
    <String, dynamic>{
      'operatorName': instance.operatorName,
      'signalQuality': instance.signalQuality,
    };
