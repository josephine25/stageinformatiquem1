import 'package:json_annotation/json_annotation.dart';

part 'network_quality.g.dart';

@JsonSerializable(explicitToJson: true)
class GlobalNetworkInfo {
  final List<OperatorAvgQuality> voiceAndSms;

  GlobalNetworkInfo(
      {required this.voiceAndSms});

  factory GlobalNetworkInfo.fromJson(Map<String, dynamic> json) =>
      _$GlobalNetworkInfoFromJson(json);
  Map<String, dynamic> toJson() => _$GlobalNetworkInfoToJson(this);
}

@JsonSerializable()
class OperatorAvgQuality {
  final String operatorName;
  final double signalQuality;
  OperatorAvgQuality(this.operatorName, this.signalQuality);

  factory OperatorAvgQuality.fromJson(Map<String, dynamic> json) =>
      _$OperatorAvgQualityFromJson(json);
  Map<String, dynamic> toJson() => _$OperatorAvgQualityToJson(this);
}