import 'package:aduitor_mobile/bloc/network_quality/events.dart';
import 'package:aduitor_mobile/bloc/network_quality/states.dart';
import 'package:aduitor_mobile/model/network_quality/network_quality.dart';
import 'package:aduitor_mobile/service/network_quality_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NetworkQualityBloc
    extends Bloc<NetworkQualityEvent, NetworkQualityState> {
  final NetworkQualityService networkQualityService =
      new NetworkQualityService();

  NetworkQualityBloc() : super(NetworkQualityInitialState());

  @override
  Stream<NetworkQualityState> mapEventToState(event) async* {
    print("-------------------$event---------------------------");
    switch (event) {
      case NetworkQualityEvent.APP_STARTED:
        //print("Appel au service de network coverage + mise en cache");
        //networkQualityService.getGlobalNetworkQuality();
        break;
      case NetworkQualityEvent.SCHEDULER_TRIGGERED:
        print("Envoi de la qualité locale à l'api de collecte");
        networkQualityService.fetchAndSendLocalNetworkQualityInfo();
        break;
      case NetworkQualityEvent.VIEW_DISPLAYED:
        print("Appel au service de network coverage + mise en cache");
        yield NetworkQualityLoading();// tourne loader dans 
        GlobalNetworkInfo networkQuality =
            await networkQualityService.getGlobalNetworkQuality();
        yield NetworkQualityLoaded(networkQuality);
        break;
      default:
    }
  }
}
