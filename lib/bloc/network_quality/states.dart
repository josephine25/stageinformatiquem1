import 'package:aduitor_mobile/model/network_quality/network_quality.dart';

abstract class NetworkQualityState {}

class NetworkQualityLoaded extends NetworkQualityState {
  final GlobalNetworkInfo networkQuality;
  NetworkQualityLoaded(this.networkQuality);
}

class NetworkQualityLoading extends NetworkQualityState {}

class NetworkQualityInitialState extends NetworkQualityState {}
