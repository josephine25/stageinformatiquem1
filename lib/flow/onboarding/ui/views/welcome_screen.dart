import 'package:aduitor_mobile/home_page.dart';
import 'package:aduitor_mobile/ui/views/discussion_view.dart';
import 'package:flutter/material.dart';
import 'package:aduitor_mobile/design_system/colorfonts.dart';
import 'package:aduitor_mobile/ui/components/action_Button.dart';
import 'package:aduitor_mobile/ui/components/indicator.dart';
import 'package:aduitor_mobile/ui/components/bottom_Container.dart';
import 'package:flutter/cupertino.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  //l'indication de navigation au niveau des onboardings 'appel de la class Indicator
  final Indicator _indicOn = new Indicator(activ: true);
  final Indicator _indicOff = new Indicator(activ: false);

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 1; i <= _numPages; i++) {
      list.add(i == _currentPage ? _indicOn : _indicOff);
    }
    return list;
  }

  var tabDescription = [
    "Découvre ce que tu peux faire",
    "Envoyez et recevez de l\'argent par mobile money et retrouvez l\'historique de vos transactions à tout moment de la journée",
    "Programmez en avance vos virements mensuels pour le loyer, l\'électricté, l\'eau et bien d\'autres encore",
    "Entre amis ou en  famille, cotisez ensemble pour les causes qui vous tiennent à coeur."
  ];
  var textTitre = [
    "Bonjour et bienvenue.",
    "Je gère mon argent de manière sécurisée",
    "Je planifie mes virements périodiques.",
    "Je cotise de l\'argent avec mes proches."
  ];
  var tabText = ["Commencer", "Suivant", "Suivant", "Terminer"];
  var couleurBoutton = [
    ThemeConstants.goblin,
    ThemeConstants.newOrleans,
    ThemeConstants.newOrleans,
    ThemeConstants.goblin
  ];
  var tabStyle = [
    ThemeConstants.bouttonTextVert,
    ThemeConstants.bouttonTextOrange,
    ThemeConstants.bouttonTextOrange,
    ThemeConstants.bouttonTextVert,
  ];
  @override
  Widget build(BuildContext context) {
    //Class Boutton
    final ActionBoutton boutton = new ActionBoutton(
        couleurBoutton: couleurBoutton[_currentPage],
        text: tabText[_currentPage],
        styleText: tabStyle[_currentPage]);
    //Class BottomContainer

    final BottomContainer containerBottom = new BottomContainer(
      hauteur: 350,
      child: contenantChild(
          _pageController,
          _buildPageIndicator(),
          _currentPage,
          boutton,
          textTitre[_currentPage],
          tabDescription[_currentPage],
          _numPages,
          context),
    );
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: ThemeConstants.newOrleans[50],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    'Passer',
                    textAlign: TextAlign.left,
                    style: ThemeConstants.skipText,
                  ),
                ],
              ),
            ),
            Container(
              height: 345.0,
              child: PageView(
                //Utilisé pour faire les slides et l'effet caroussel
                physics: ClampingScrollPhysics(),
                controller: _pageController,
                onPageChanged: (int page) {
                  setState(() {
                    _currentPage = page;
                  });
                },
                children: <Widget>[
                  Container(
                    child: Image(
                      image: AssetImage(
                        'assets/images/welcome.png',
                      ),
                      height: 345.0,
                      width: 345.0,
                    ),
                  ),
                  Container(
                    child: Image(
                      image: AssetImage(
                        'assets/images/manage_money.png',
                      ),
                      height: 345.0,
                      width: 345.0,
                    ),
                  ),
                  Container(
                    child: Image(
                      image: AssetImage(
                        'assets/images/plan_transferts.png',
                      ),
                      height: 345.0,
                      width: 345.0,
                    ),
                  ),
                  Container(
                    child: Image(
                      image: AssetImage(
                        'assets/images/contribute_money.png',
                      ),
                      height: 345.0,
                      width: 345.0,
                    ),
                  ),
                ],
              ),
            ),
            Stack(children: [
              containerBottom,
            ]),
          ],
        ),
      ),
    );
  }
}

contenantChild(
    PageController pageController,
    List<Widget> liste,
    int currentPage,
    ActionBoutton boutton,
    String textTitre,
    String tabDescription,
    int numPage,
    BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      SizedBox(
        height: 3,
      ),
      Container(
          height: 9.0,
          child: currentPage == 0
              ? Text('')
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: liste,
                )),
      SizedBox(
        height: 3,
      ),
      Center(
        child: Text(textTitre,
            textAlign: TextAlign.center, style: ThemeConstants.headline4),
      ),
      SizedBox(
        height: 8,
      ),
      Text(tabDescription,
          textAlign: TextAlign.center, style: ThemeConstants.bodyRegularStyle),
      SizedBox(
        height: 15,
      ),
      GestureDetector(
        onTap: () {
          (currentPage + 1) == 4
              ? Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => MyHomePage()))
              : pageController.nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.ease,
                );
        },
        child: boutton,
      )
    ],
  );
}
