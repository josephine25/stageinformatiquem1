// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_quality_service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalNetworkQualityInfo _$LocalNetworkQualityInfoFromJson(
    Map<String, dynamic> json) {
  return LocalNetworkQualityInfo(
    mobileOperatorInfoList: (json['mobileOperatorInfoList'] as List<dynamic>)
        .map((e) => MobileOperatorInfo.fromJson(e as Map<String, dynamic>))
        .toList(),
    longitude: (json['longitude'] as num).toDouble(),
    latitude: (json['latitude'] as num).toDouble(),
  );
}

Map<String, dynamic> _$LocalNetworkQualityInfoToJson(
        LocalNetworkQualityInfo instance) =>
    <String, dynamic>{
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'mobileOperatorInfoList': instance.mobileOperatorInfoList,
    };

MobileOperator _$MobileOperatorFromJson(Map<String, dynamic> json) {
  return MobileOperator(
    mnc: json['mnc'] as String,
    mcc: json['mcc'] as String,
  );
}

Map<String, dynamic> _$MobileOperatorToJson(MobileOperator instance) =>
    <String, dynamic>{
      'mnc': instance.mnc,
      'mcc': instance.mcc,
    };

MobileOperatorInfo _$MobileOperatorInfoFromJson(Map<String, dynamic> json) {
  return MobileOperatorInfo(
    mobileOperator:
        MobileOperator.fromJson(json['mobileOperator'] as Map<String, dynamic>),
    signalStrengthDbm: json['signalStrengthDbm'] as String,
  );
}

Map<String, dynamic> _$MobileOperatorInfoToJson(MobileOperatorInfo instance) =>
    <String, dynamic>{
      'mobileOperator': instance.mobileOperator,
      'signalStrengthDbm': instance.signalStrengthDbm,
    };
