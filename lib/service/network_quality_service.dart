import 'package:aduitor_mobile/model/network_quality/network_quality.dart';
import 'package:aduitor_mobile/service/client/network_quality_client.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:json_annotation/json_annotation.dart';

part 'network_quality_service.g.dart';

class NetworkQualityService {
  NetworkQualityClient networkQualityClient = NetworkQualityClient();
  static const platform = const MethodChannel('mobile.arptc.dev/network');

  Future<GlobalNetworkInfo> getGlobalNetworkQuality() async {
    Position gpsPosition = await _determineGPSPosition();

    print("longitude: ${gpsPosition.longitude.toString()}");
    print("latitude: ${gpsPosition.latitude.toString()}");
    var globalNetworkQuality = networkQualityClient.getGlobalNetworkQuality(
        longitude: gpsPosition.longitude,
        latitude: gpsPosition.latitude,
        radius: 300);
    return globalNetworkQuality;

    // Mock provisoire en attendant le développement de l'api
    /* return GlobalNetworkInfo(voiceAndSms: [
      OperatorAvgQuality("Orange", 0.75),
      OperatorAvgQuality("Vodacom", 0.90),
      OperatorAvgQuality("Africel", 0.60),
      OperatorAvgQuality("Airtel", 0.25),
    ]);*/
  }

  fetchAndSendLocalNetworkQualityInfo() async {
    LocalNetworkQualityInfo qualityInfo = await _fetchLocalNetworkQualityInfo();
    networkQualityClient.sendLocalNetworkQualityInfo(qualityInfo);
  }

  Future<LocalNetworkQualityInfo> _fetchLocalNetworkQualityInfo() async {
    Position gpsPosition = await _determineGPSPosition();

    List<MobileOperatorInfo> mobileOperatorInfoList =
    await _getLocalOperatorInfo();

    return LocalNetworkQualityInfo(
        longitude: gpsPosition.longitude,
        latitude: gpsPosition.latitude,
        mobileOperatorInfoList: mobileOperatorInfoList);
  }

  Future<List<MobileOperatorInfo>> _getLocalOperatorInfo() async {
    final List<dynamic> result = await platform.invokeMethod('getNetworkInfo');

    /*String dataNetworkType = result
        .where((operatorInfo) => operatorInfo["dataNetworkType"] != null)
        .map((operatorInfo) => operatorInfo["dataNetworkType"].toString())
        .first;

    print("Network Type :" + dataNetworkType);*/

    return result
        .where((operatorInfo) =>
    operatorInfo["mnc"] != null && operatorInfo["mcc"] != null)
        .map((operatorInfo) =>
        MobileOperatorInfo(
            mobileOperator: MobileOperator(
                mnc: operatorInfo["mnc"], mcc: operatorInfo["mcc"]),
            signalStrengthDbm: operatorInfo["signalStrength"]))
        .toList();
  }

  Future<Position> _determineGPSPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    return await Geolocator.getCurrentPosition();
  }
}

@JsonSerializable()
class LocalNetworkQualityInfo {
  final double longitude;
  final double latitude;
  final List<MobileOperatorInfo> mobileOperatorInfoList;

  LocalNetworkQualityInfo({required this.mobileOperatorInfoList,
    required this.longitude,
    required this.latitude});

  factory LocalNetworkQualityInfo.fromJson(Map<String, dynamic> json) =>
      _$LocalNetworkQualityInfoFromJson(json);

  Map<String, dynamic> toJson() => _$LocalNetworkQualityInfoToJson(this);
}

@JsonSerializable()
class MobileOperator {
  final String mnc;
  final String mcc;

  MobileOperator({required this.mnc, required this.mcc});

  factory MobileOperator.fromJson(Map<String, dynamic> json) =>
      _$MobileOperatorFromJson(json);

  Map<String, dynamic> toJson() => _$MobileOperatorToJson(this);
}

@JsonSerializable()
class MobileOperatorInfo {
  final MobileOperator mobileOperator;
  final String signalStrengthDbm;

  MobileOperatorInfo(
      {required this.mobileOperator, required this.signalStrengthDbm});

  factory MobileOperatorInfo.fromJson(Map<String, dynamic> json) =>
      _$MobileOperatorInfoFromJson(json);

  Map<String, dynamic> toJson() => _$MobileOperatorInfoToJson(this);
}
