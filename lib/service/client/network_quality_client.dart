import 'dart:convert';
import 'dart:io';

import 'package:aduitor_mobile/model/network_quality/network_quality.dart';
import 'package:aduitor_mobile/service/network_quality_service.dart';
import 'package:http/io_client.dart';

class NetworkQualityClient {
  final IOClient ioClient = buildIoClient(true);

  Future<void> sendLocalNetworkQualityInfo(
      LocalNetworkQualityInfo localNetworkQualityInfo) async {
    var url = Uri.parse(
        "https://104.131.69.81/network-quality-app/api/send-network-info");
    ioClient.post(
      url,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(localNetworkQualityInfo.toJson()),
    );
  }

  Future<GlobalNetworkInfo> getGlobalNetworkQuality(
      {required double longitude,
      required double latitude,
      required int radius}) async {
    var queryParameters = {
      "longitude": longitude.toString(),
      "latitude": latitude.toString(),
      "radius": radius.toString()
    };

    /*var url = Uri.parse(
        "https://localhost:8080/api/get-global-network-quality?"+
            "longitude=${longitude.toString()}&latitude=${latitude.toString()}&radius=$radius");*/

    var url = Uri.https("104.131.69.81",
        "/network-quality-app/api/get-global-network-quality", queryParameters);

    print(url.toString());

    var response = await ioClient.get(url);
    return GlobalNetworkInfo.fromJson(jsonDecode(response.body));
  }

  static IOClient buildIoClient(bool trustSelfSigned) {
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);
    return ioClient;
  }
}
