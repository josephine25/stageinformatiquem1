import 'package:aduitor_mobile/bloc/network_quality/bloc.dart';
import 'package:aduitor_mobile/bloc/network_quality/events.dart';
import 'package:aduitor_mobile/scheduler.dart';
import 'package:aduitor_mobile/ui/views/discussion_view.dart';
import 'package:aduitor_mobile/ui/views/network_quality_view.dart';
import 'package:aduitor_mobile/ui/views/feature_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workmanager/workmanager.dart';
import "package:flutter_feather_icons/flutter_feather_icons.dart";

import 'design_system/colorfonts.dart';

const String TAG = "BackGround_Work";

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title}) : super(key: key);
  final String? title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _navigationSelectedIndex = 0;
  @override
  void initState() {
    super.initState();
    var networkQualityBloc = context.read<NetworkQualityBloc>();
    networkQualityBloc.add(NetworkQualityEvent.APP_STARTED);
    Workmanager().registerPeriodicTask(
      TAG,
      "simplePeriodicTask",
      initialDelay: Duration(seconds: 1),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _navigationSelectedIndex = index;
      if (_navigationSelectedIndex == 2) {
        var networkQualityBloc = context.read<NetworkQualityBloc>();
        networkQualityBloc.add(NetworkQualityEvent.VIEW_DISPLAYED);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final FeatureView currentView;

    switch (_navigationSelectedIndex) {
      case 0:
        currentView = FeatureView('Discussions', content: Discussion());
        break;
      case 1:
        currentView = FeatureView('Notification de surconsommation');
        break;
      case 2:
        currentView = FeatureView('Evolution qualité signal',
            content: NetworkQualityView());
        break;
      case 3:
        currentView = FeatureView('Données personnelles');
        break;
      case 4:
        currentView = FeatureView("Suivi de la consommation");
        break;
      default:
        throw Exception("Index not managed");
    }

    return mainNavigation(currentView);
  }

  Widget mainNavigation(FeatureView currentView) {
    return Scaffold(
      appBar: AppBar(
          toolbarHeight: 65.0,
          automaticallyImplyLeading: false,
          title: Text(currentView.title),
          elevation: 0.0,
          actions: [
            // Icon(FeatherIcons.plus),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 7),
                child: currentView.title == 'Discussions'
                    ? Icon(FeatherIcons.plus)
                    : Text("")),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Icon(FeatherIcons.bell),
            ),
          ],
          backgroundColor: Colors.white),
      body: currentView,
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(FeatherIcons.send),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(FeatherIcons.dollarSign),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(FeatherIcons.barChart2),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(FeatherIcons.gift),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(FeatherIcons.user),
            label: "",
          ),
        ],
        onTap: _onItemTapped,
        currentIndex: _navigationSelectedIndex,
      ),
    );
  }
}
